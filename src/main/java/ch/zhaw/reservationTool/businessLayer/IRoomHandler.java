package ch.zhaw.reservationTool.businessLayer;

import ch.zhaw.reservationTool.dataObjects.dbEntities.Room;

import java.util.List;
import java.util.Map;

/**
 * Created by raphael on 14.12.13.
 */
public interface IRoomHandler {

    List<Room> getRooms();

    public void saveRoom(String roomName);

    public Map<Integer, String> getRoomsAsMap();
}
