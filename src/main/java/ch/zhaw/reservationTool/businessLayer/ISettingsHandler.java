package ch.zhaw.reservationTool.businessLayer;

/**
 * Created by raphael on 17.12.13.
 */
public interface ISettingsHandler {
    public String getSettingString(String settingName);

    public void reloadSettings() throws Exception;

    public boolean settingExists(String settingName);

    public boolean getSettingBoolean(String settingName);
}
