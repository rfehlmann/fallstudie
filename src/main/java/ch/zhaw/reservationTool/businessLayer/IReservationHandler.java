package ch.zhaw.reservationTool.businessLayer;

import ch.zhaw.reservationTool.dataObjects.dbEntities.Reservation;
import ch.zhaw.reservationTool.dataObjects.dbEntities.Room;

import java.util.Date;
import java.util.List;

/**
 * Created by raphael on 14.12.13.
 */
public interface IReservationHandler {
    public List<Reservation> getReservations(Date startDate, List<Room> rooms);

    void save(Reservation reservation) throws Exception;
}
