package ch.zhaw.reservationTool.businessLayer;

import ch.zhaw.reservationTool.dataObjects.dbEntities.DbEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 22.11.13
 * Time: 18:21
 * To change this template use File | Settings | File Templates.
 */
public interface IDatabaseHandler {

    /**
     * get database entry
     * @param table database table name
     * @return  a list with the database entrys
     */
    public ArrayList<Object> listElements(String table) throws HibernateException;

    public ArrayList<?> listElements(String sql, Map<String, Object> parameters, Class map) throws HibernateException;

    public Session openSession()throws HibernateException;

    public void closeSession()throws HibernateException;

    public void updateDbEntity(DbEntity dbEntity) throws Exception;

    public void saveDbEntity(DbEntity dbEntity) throws Exception;

    public void deleteDbEntity(DbEntity dbEntity) throws Exception;
}



