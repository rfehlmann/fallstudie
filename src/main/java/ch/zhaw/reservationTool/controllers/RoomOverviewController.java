package ch.zhaw.reservationTool.controllers;

import ch.zhaw.reservationTool.dataObjects.beans.ReservationFormBean;
import ch.zhaw.reservationTool.exception.SessionException;
import ch.zhaw.reservationTool.handler.InstanceManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: pegi
 * Date: 1/5/14
 * Time: 3:19 AM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class RoomOverviewController extends BaseController {
    Logger logger = Logger.getLogger(RoomOverviewController.class);

    @Autowired
    private HttpServletRequest request;


    @RequestMapping(value = "/showrooms", method = RequestMethod.GET)
    public ModelAndView showRoomOverview() throws SessionException {

        //add url and Bean to save the form Values
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("roomOverview");
        InstanceManager manager = super.getManager(request);
        //get Rooms
        modelAndView.addObject("rooms", manager.getRoomHandler().getRoomsAsMap());

        return modelAndView;

    }

    @RequestMapping(value = "/saveRoom", method = RequestMethod.POST)
    public ModelAndView saveReservation(ReservationFormBean reservationFormBean) throws SessionException {
        try {

            String dateString = reservationFormBean.getDate();
            String start = reservationFormBean.getStartTime();
            String end = reservationFormBean.getEndTime();

            return this.showRoomOverview();
        } catch (Exception e) {
            return super.showErrorPage(e, request);
        }

    }
}
