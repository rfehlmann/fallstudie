package ch.zhaw.reservationTool.controllers;

import ch.zhaw.reservationTool.dataObjects.beans.ReservationBean;
import ch.zhaw.reservationTool.dataObjects.dbEntities.Reservation;
import ch.zhaw.reservationTool.dataObjects.dbEntities.Room;
import ch.zhaw.reservationTool.exception.SessionException;
import ch.zhaw.reservationTool.handler.InstanceManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 15:12
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class OverViewController extends BaseController {
    Logger logger = Logger.getLogger(OverViewController.class);

    @Autowired
    HttpServletRequest request;

    @RequestMapping(value = "/overview", method = RequestMethod.GET)
    public ModelAndView showOverViewPage() throws SessionException {
        InstanceManager instanceManager = super.getManager(request);
        ModelAndView modelAndView = new ModelAndView("overview");
        Object startDateObject = request.getParameter("startDate");
        Object currentDate = request.getParameter("currentDate");
        Date date = new Date();
        if (startDateObject != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(Long.valueOf(currentDate.toString())));
            cal.add(Calendar.DATE, Integer.valueOf(startDateObject.toString())); //minus number would decrement the days
            date = cal.getTime();

        }
        List<Room> roomList = instanceManager.getRoomHandler().getRooms();
        List<Reservation> reservations = instanceManager.getReservationHandler().getReservations(date, roomList);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MMM.yyyy");
        modelAndView.addObject("date", simpleDateFormat.format(date));
        String timeStamp = String.valueOf(date.getTime());
        modelAndView.addObject("timeStamp", timeStamp);
        modelAndView.addObject("reservations", this.fillReservationBeans(roomList, reservations));
        return modelAndView;

    }

    private List<ReservationBean> fillReservationBeans(List<Room> rooms, List<Reservation> reservations) {
        Map<Integer, List<Reservation>> mapRoomIdToName = new HashMap<>();
        List<ReservationBean> reservationBeanMap = new ArrayList<>();
        List<ReservationBean> beans = new ArrayList<>();
        for (Reservation reservation : reservations) {
            if (mapRoomIdToName.containsKey(reservation.getIdrRoom())) {
                mapRoomIdToName.get(reservation.getIdrRoom()).add(reservation);
            } else {
                List<Reservation> reservationList = new ArrayList<>();
                reservationList.add(reservation);
                mapRoomIdToName.put(reservation.getIdrRoom(), reservationList);
            }
        }

        for (Room room : rooms) {
            ReservationBean reservationBean = new ReservationBean();
            reservationBean.setRoomName(room.getRoomName());
            if (mapRoomIdToName.containsKey(room.getIdRoom())) {
                List<Reservation> reservationList = mapRoomIdToName.get(room.getIdRoom());
                for (Reservation reservation : reservationList) {
                    reservationBean.putElementStartDate(reservation.getStartDate().getHours(), String.valueOf(reservation.getEndDate().getHours()));

                }
            }

            reservationBeanMap.add(reservationBean);
        }
        return reservationBeanMap;
    }
}
