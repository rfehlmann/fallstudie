package ch.zhaw.reservationTool.controllers;

import ch.zhaw.reservationTool.dataObjects.beans.ReservationFormBean;
import ch.zhaw.reservationTool.dataObjects.dbEntities.Reservation;
import ch.zhaw.reservationTool.exception.SessionException;
import ch.zhaw.reservationTool.handler.InstanceManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by raphael on 14.12.13.
 */
@Controller
public class ReservationController extends BaseController {
    Logger logger = Logger.getLogger(ReservationController.class);

    @Autowired
    private HttpServletRequest request;


    @RequestMapping(value = "/showaddfeservationform", method = RequestMethod.GET)
    public ModelAndView showAddReservationForm() throws SessionException {

        //add url and Bean to save the form Values
        ModelAndView modelAndView = new ModelAndView("reservationForm", "command", new ReservationFormBean());
        InstanceManager manager = super.getManager(request);
        //get Rooms
        modelAndView.addObject("rooms", manager.getRoomHandler().getRoomsAsMap());

        return modelAndView;

    }

    @RequestMapping(value = "/saveReservation", method = RequestMethod.POST)
    public ModelAndView saveReservation(@ModelAttribute ReservationFormBean reservationFormBean) throws SessionException {
        try {

            String dateString = reservationFormBean.getDate();
            DateFormat df = new SimpleDateFormat("mm/dd/yyyy");
            Date date = df.parse(dateString);

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            String startHour = reservationFormBean.getStartTime();

            String endHour = reservationFormBean.getEndTime();
            cal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(endHour));
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            Date endDate = cal.getTime();
            cal.set(Calendar.HOUR_OF_DAY, Integer.valueOf(startHour));
            Date startDate = cal.getTime();
            Reservation reservation = new Reservation();
            reservation.setEndDate(new Timestamp(endDate.getTime()));
            reservation.setIdrRoom(Integer.valueOf(reservationFormBean.getRoomId()));
            reservation.setStartDate(new Timestamp(startDate.getTime()));
            super.getManager(request).getReservationHandler().save(reservation);
            ModelAndView modelAndView = new ModelAndView("stringValue");
            modelAndView.addObject("message", "Daten erfolgreich gespeichert");

            return modelAndView;
        } catch (Exception e) {
            e.printStackTrace();
            return super.showErrorPage(e, request);
        }

    }
}
