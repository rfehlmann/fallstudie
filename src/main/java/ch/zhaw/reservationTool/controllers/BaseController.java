package ch.zhaw.reservationTool.controllers;

import ch.zhaw.reservationTool.dataObjects.Session;
import ch.zhaw.reservationTool.exception.SessionException;
import ch.zhaw.reservationTool.handler.InstanceManager;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 18:07
 * To change this template use File | Settings | File Templates.
 */
public class BaseController {
    Logger logger = Logger.getLogger(BaseController.class);

    public InstanceManager getManager(HttpServletRequest httpServletRequest) throws SessionException {
        try {
            Object sessionObject = httpServletRequest.getSession().getAttribute("userSession");
            if (sessionObject instanceof Session) {
                return ((Session) sessionObject).getInstanceManager();
            }
        } catch (IllegalStateException e) {
            throw new SessionException(e);
        }
        return null;
    }

    public ModelAndView showErrorPage(Exception e, HttpServletRequest request) {
        boolean showStackTrace = false;
        try {
            if (this.getManager(request).getSettingsHandler().settingExists("errorhandling.showStacktrace")) {
                showStackTrace = this.getManager(request).getSettingsHandler().getSettingBoolean("errorhandling.showStacktrace");
            }
        } catch (Exception exe) {
            logger.error("error loading instance manager");
        }
        //create model and view and set load the error.jsp
        ModelAndView model = new ModelAndView("error");
        if (showStackTrace) {
            model.addObject("stacktrace", ExceptionUtils.getStackTrace(e));
        }
        model.addObject("message", e.getMessage());
        return model;
    }
}
