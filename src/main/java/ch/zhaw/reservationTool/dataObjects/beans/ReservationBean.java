package ch.zhaw.reservationTool.dataObjects.beans;

import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 19:13
 * To change this template use File | Settings | File Templates.
 */
public class ReservationBean {
    Logger logger = Logger.getLogger(ReservationBean.class);
    String roomName = "";
    HashMap<Integer, String> startEndMapping = new HashMap<>();

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public HashMap<Integer, String> getStartEndMapping() {
        return startEndMapping;
    }

    public void setStartEndMapping(HashMap<Integer, String> startEndMapping) {
        this.startEndMapping = startEndMapping;
    }

    public void putElementStartDate(Integer start, String end) {
        this.startEndMapping.put(start,end);
    }
}
