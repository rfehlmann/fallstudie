package ch.zhaw.reservationTool.dataObjects.beans;

import org.apache.log4j.Logger;

/**
 * Created by raphael on 14.12.13.
 */
public class ReservationFormBean {
    Logger logger = Logger.getLogger(ReservationFormBean.class);

    //roomid
    String roomId = "";
    //timestamp from resveration date
    String date = "";
    //hours where the meeting start and ends
    String startTime, endTime;

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
