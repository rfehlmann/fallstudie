package ch.zhaw.reservationTool.dataObjects.dbEntities;

import org.apache.log4j.Logger;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 19:09
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "room")
public class Room implements DbEntity {
    Logger logger = Logger.getLogger(Room.class);
    private int idRoom;
    private String roomName;

    @javax.persistence.Column(name = "id_room", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    @javax.persistence.Column(name = "room_name", nullable = false, insertable = true, updatable = true, length = 255, precision = 0)
    @Basic
    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        if (idRoom != room.idRoom) return false;
        if (roomName != null ? !roomName.equals(room.roomName) : room.roomName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idRoom;
        result = 31 * result + (roomName != null ? roomName.hashCode() : 0);
        return result;
    }
}
