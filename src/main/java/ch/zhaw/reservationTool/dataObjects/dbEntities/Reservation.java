package ch.zhaw.reservationTool.dataObjects.dbEntities;

import org.apache.log4j.Logger;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 19:09
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "reservation")

public class Reservation implements DbEntity {
    Logger logger = Logger.getLogger(Reservation.class);
    private int idReservation;
    private int idrRoom;
    private Timestamp startDate;
    private Timestamp endDate;

    @javax.persistence.Column(name = "id_reservation", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(int idReservation) {
        this.idReservation = idReservation;
    }

    @javax.persistence.Column(name = "idr_room", nullable = false, insertable = true, updatable = true, length = 10, precision = 0)
    @Basic
    public int getIdrRoom() {
        return idrRoom;
    }

    public void setIdrRoom(int idrRoom) {
        this.idrRoom = idrRoom;
    }

    @javax.persistence.Column(name = "start_date", nullable = true, insertable = true, updatable = true, length = 19, precision = 0)
    @Basic
    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    @javax.persistence.Column(name = "end_date", nullable = true, insertable = true, updatable = true, length = 19, precision = 0)
    @Basic
    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reservation that = (Reservation) o;

        if (idReservation != that.idReservation) return false;
        if (idrRoom != that.idrRoom) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idReservation;
        result = 31 * result + idrRoom;
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        return result;
    }

}
