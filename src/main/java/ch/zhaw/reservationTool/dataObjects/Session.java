package ch.zhaw.reservationTool.dataObjects;

import ch.zhaw.reservationTool.handler.InstanceManager;
import org.apache.log4j.Logger;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 17:54
 * To change this template use File | Settings | File Templates.
 */
public class Session {
    Logger logger = Logger.getLogger(Session.class);

    // instances
    private InstanceManager instanceManager = null;
    private HttpSession httpSession= null;
    public Session(HttpSession httpSession){
       this.httpSession = httpSession;
        this.init();
    }

   private   void init(){
       if(this.instanceManager == null){
           this.instanceManager = new InstanceManager();
       }
   }

    public InstanceManager getInstanceManager() {
        return instanceManager;
    }
}
