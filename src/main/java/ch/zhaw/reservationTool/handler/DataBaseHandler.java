package ch.zhaw.reservationTool.handler;

import ch.zhaw.reservationTool.businessLayer.IDatabaseHandler;
import ch.zhaw.reservationTool.dataObjects.dbEntities.DbEntity;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 19:33
 * To change this template use File | Settings | File Templates.
 */
public class DataBaseHandler implements IDatabaseHandler {
    Logger logger = Logger.getLogger(DataBaseHandler.class);
    private Session session;

    public DataBaseHandler() {
        this.buildSessionFactory();
    }

    public ArrayList<Object> listElements(String table) throws HibernateException {
        if (!table.startsWith("from")) {
            table = "from " + table;
        }
        ArrayList<Object> elements = new ArrayList<Object>();

        List items = this.openSession().createQuery(table).list();
        for (Object o : items) {
            // if(o instanceof DataBaseEntity){
            elements.add(o);
        }
        // }
        return elements;
    }

    public ArrayList<?> listElements(String sql, Map<String, Object> parameters, Class map) {

        ArrayList<Object> elements = new ArrayList<Object>();
        SQLQuery query = this.createQuery(sql, parameters);
        query.addEntity(map);
        List items = query.list();
        for (Object o : items) {
            // if(o instanceof DataBaseEntity){
            elements.add(o);
        }
        // }
        return elements;
    }

    private SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public Session openSession() throws HibernateException {
        if (this.session == null || !this.session.isOpen()) {
            this.session = this.getSessionFactory().openSession();
        }
        return session;
    }

    public void closeSession() throws HibernateException {
        this.openSession().close();
    }

    private SessionFactory sessionFactory = null;

    private SessionFactory buildSessionFactory() {
        try {
            URL u = getClass().getResource("/hibernate.xml");
            File f = new File(u.getFile());
            // Create the SessionFactory from hibernate.cfg.xml
            Configuration config = new Configuration();
            //   f = new File(f.getAbsolutePath()+File.separator+"hibernate.xml");
            config.configure(f);
            ServiceRegistry serviceRegistry = new ServiceRegistryBuilder().applySettings(config.getProperties()).buildServiceRegistry();
            sessionFactory = config.buildSessionFactory(serviceRegistry);
            return sessionFactory;
        } catch (Throwable ex) {
            throw new ExceptionInInitializerError(ex);
        }
    }

    private SQLQuery createQuery(String sql, Map<String, Object> values) throws HibernateException {
        org.hibernate.SQLQuery query = this.openSession()
                .createSQLQuery(sql);
        for (String key : values.keySet()) {
            if (sql.contains(key)) {
                Object value = values.get(key);
                if (value instanceof Date) {
                    query.setTimestamp(key, (Date) value);
                } else
                    query.setParameter(key, value);
            }
        }
        return query;
    }

    public void saveDbEntity(DbEntity dbEntity) throws Exception {
        try {
            this.openSession().beginTransaction();
            this.openSession().save(dbEntity);
            this.openSession().getTransaction().commit();

        } catch (Exception e) {
            this.openSession().getTransaction().rollback();
            logger.error("error on delete:", e);
            throw e;
        }
    }

    public void updateDbEntity(DbEntity dbEntity) throws Exception {
        try {
            this.openSession().beginTransaction();
            this.openSession().merge(dbEntity);
            this.openSession().getTransaction().commit();

        } catch (Exception e) {
            this.openSession().getTransaction().rollback();
            logger.error("error on delete:", e);
            throw e;
        }
    }

    public void deleteDbEntity(DbEntity dbEntity) throws Exception {
        try {
            this.openSession().beginTransaction();
            this.openSession().delete(dbEntity);
            this.openSession().getTransaction().commit();

        } catch (Exception e) {
            this.openSession().getTransaction().rollback();
            logger.error("error on delete:", e);
            throw e;
        }
    }
}
