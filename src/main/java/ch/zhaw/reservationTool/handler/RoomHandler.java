package ch.zhaw.reservationTool.handler;

import ch.zhaw.reservationTool.businessLayer.IRoomHandler;
import ch.zhaw.reservationTool.dataObjects.dbEntities.Room;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
public class RoomHandler extends BaseHandler implements IRoomHandler {
    Logger logger = Logger.getLogger(RoomHandler.class);
    private final String selectSQL = "from Room";

    public RoomHandler(InstanceManager manager) {
        super(manager);
    }

    public List<Room> getRooms() {
        List<Room> rooms = new ArrayList<>();
        List<Object> dbRes = super.instanceManager.getDataBaseHandler().listElements(selectSQL);
        if (dbRes != null) {
            for (Object row : dbRes) {
                if (row instanceof Room) {
                    rooms.add((Room) row);
                }
            }
        }
        return rooms;
    }

    public void saveRoom(String roomName) {
        Room room = new Room();
        room.setRoomName(roomName);

    }

    public Map<Integer, String> getRoomsAsMap() {
        Map<Integer, String> rooms = new HashMap<>();
        List<Room> roomList = this.getRooms();
        for (Room room : roomList) {
            rooms.put(room.getIdRoom(), room.getRoomName());
        }
        return rooms;
    }
}
