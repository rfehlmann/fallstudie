package ch.zhaw.reservationTool.handler;

import ch.zhaw.reservationTool.businessLayer.ISettingsHandler;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by raphael on 17.12.13.
 */
public class SettingsHandler extends BaseHandler implements ISettingsHandler {
    Logger logger = Logger.getLogger(SettingsHandler.class);
    private Map<String, String> settings = new HashMap<>();

    public SettingsHandler(InstanceManager manager) {
        super(manager);
        try {
            this.loadSettings();
        } catch (Exception e) {
            logger.error("can not read settings", e);
        }
    }

    private File getSettingsFile() {
        URL u = getClass().getResource("/settings.properties");
        File f = new File(u.getFile());
        return f;
    }

    private void loadSettings() throws Exception {
        File f = this.getSettingsFile();
        if (!f.exists() || !f.canRead()) {
            throw new FileNotFoundException("settings file not found");
        }
        Properties properties = new Properties();
        try {
            properties.load(new FileReader(f));
        } catch (Exception e) {
            logger.error("can not read settings", e);
            throw new Exception(e);
        }

        for (String name : properties.stringPropertyNames()) {
            this.settings.put(name, properties.getProperty(name));
        }
    }

    public String getSettingString(String settingName) {
        return this.settings.get(settingName);
    }

    public void reloadSettings() throws Exception {
        this.loadSettings();
    }

    public boolean settingExists(String settingName) {
        return this.settings.containsKey(settingName);
    }

    public boolean getSettingBoolean(String settingName) {
        String settingValue = this.getSettingString(settingName);
        return settingValue != null && settingValue.toLowerCase().equals("true");
    }
}
