package ch.zhaw.reservationTool.handler;

import org.apache.log4j.Logger;

/**
 * Created by raphael on 14.12.13.
 */
public class BaseHandler {
    Logger logger = Logger.getLogger(BaseHandler.class);
    InstanceManager instanceManager = null;

    public BaseHandler(InstanceManager manager) {
        this.instanceManager = manager;
    }
}
