package ch.zhaw.reservationTool.handler;

import ch.zhaw.reservationTool.businessLayer.IDatabaseHandler;
import ch.zhaw.reservationTool.businessLayer.IReservationHandler;
import ch.zhaw.reservationTool.businessLayer.IRoomHandler;
import ch.zhaw.reservationTool.businessLayer.ISettingsHandler;
import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 15:59
 * To change this template use File | Settings | File Templates.
 */
public class InstanceManager {
    Logger logger = Logger.getLogger(InstanceManager.class);
    private IRoomHandler roomHandler = null;
    private IDatabaseHandler dataBaseHandler = null;
    private IReservationHandler reservationHandler = null;
    private ISettingsHandler settingsHandler = null;

    public InstanceManager() {
        init();
    }

    private void init() {
        if (this.roomHandler == null) {
            roomHandler = new RoomHandler(this);
        }
        if (dataBaseHandler == null) {
            dataBaseHandler = new DataBaseHandler();
        }
        if(reservationHandler == null){
            reservationHandler = new ReservationHandler(this);
        }
        if (settingsHandler == null) {
            settingsHandler = new SettingsHandler(this);
        }
    }

    public IRoomHandler getRoomHandler() {
        return roomHandler;
    }

    public IDatabaseHandler getDataBaseHandler() {
        return dataBaseHandler;
    }

    public IReservationHandler getReservationHandler() {
        return reservationHandler;
    }

    public ISettingsHandler getSettingsHandler() {
        return settingsHandler;
    }

    ;
}
