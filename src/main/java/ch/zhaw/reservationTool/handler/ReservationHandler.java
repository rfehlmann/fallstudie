package ch.zhaw.reservationTool.handler;

import ch.zhaw.reservationTool.businessLayer.IReservationHandler;
import ch.zhaw.reservationTool.dataObjects.dbEntities.Reservation;
import ch.zhaw.reservationTool.dataObjects.dbEntities.Room;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 19:18
 * To change this template use File | Settings | File Templates.
 */
public class ReservationHandler extends BaseHandler implements IReservationHandler {
    Logger logger = Logger.getLogger(ReservationHandler.class);
    private final String selectSql = "from reservation ";

    public ReservationHandler(InstanceManager manager) {
        super(manager);
    }

    public List<Reservation> getReservations(Date startDate, List<Room> rooms) {
        List<Integer> roomIds = new ArrayList<>();
        for (Room room : rooms) {
            roomIds.add(room.getIdRoom());

        }
        StringBuffer sql = new StringBuffer("select * " + selectSql);
        sql.append(" where idr_room in (" + StringUtils.join(roomIds.iterator(), ",") + ")");
        Date endDate = (Date) startDate.clone();
        startDate.setHours(0);
        startDate.setMinutes(0);
        startDate.setSeconds(0);
        endDate.setHours(23);
        endDate.setMinutes(59);
        endDate.setSeconds(59);
        sql.append(" and start_date > :startDate and start_date < :endDate");
        Map<String, Object> paramters = new HashMap<>();
        paramters.put("startDate", startDate);
        paramters.put("endDate", endDate);
        List<Reservation> reservations = (ArrayList<Reservation>) super.instanceManager.getDataBaseHandler().listElements(sql.toString(), paramters, Reservation.class);
        return reservations;
    }

    public void save(Reservation reservation) throws Exception {
        System.out.println("kkk");
        this.instanceManager.getDataBaseHandler().saveDbEntity(reservation);
    }
}
