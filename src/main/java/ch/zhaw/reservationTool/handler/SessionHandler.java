package ch.zhaw.reservationTool.handler;

import ch.zhaw.reservationTool.dataObjects.Session;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 17:53
 * To change this template use File | Settings | File Templates.
 */
public class SessionHandler implements HttpSessionListener {
    Logger logger = Logger.getLogger(SessionHandler.class);

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        Session session = new Session(httpSessionEvent.getSession());
        httpSessionEvent.getSession().setAttribute("userSession", session);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        Object session = httpSessionEvent.getSession().getAttribute("userSession");
        if (session != null) {

        }
    }
}
