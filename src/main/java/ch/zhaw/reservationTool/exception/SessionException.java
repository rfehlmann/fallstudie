package ch.zhaw.reservationTool.exception;

import org.apache.log4j.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 18:08
 * To change this template use File | Settings | File Templates.
 */
public class SessionException extends Exception {
    Logger logger = Logger.getLogger(SessionException.class);
    private final String errorMessage = "Session Exception";

    public SessionException(Exception e){
        this.setStackTrace(e.getStackTrace());
    }


    public String getErrorMessage() {
        return errorMessage;
    }
    private  void  printStackTraceToStandartOutPut(){
      this.printStackTrace();
    }
}
