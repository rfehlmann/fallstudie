DROP TABLE IF EXISTS room;
CREATE TABLE room (
  id_room   INT(11)      NOT NULL AUTO_INCREMENT PRIMARY KEY,
  room_name VARCHAR(255) NOT NULL
)
  ENGINE =INNODB;


DROP TABLE IF EXISTS reservation;

CREATE TABLE reservation (
  id_reservation INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idr_room       INT(11) NOT NULL,
  start_date     DATETIME ,
  end_date       DATETIME ,
  FOREIGN KEY (idr_room) REFERENCES room (id_room)
    ON DELETE CASCADE
)
  ENGINE =INNODB;
