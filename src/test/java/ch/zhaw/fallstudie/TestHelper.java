package ch.zhaw.fallstudie;

import ch.zhaw.reservationTool.dataObjects.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:appServlet-servlet.xml" )


public class TestHelper {
    @Autowired
    private org.springframework.web.context.WebApplicationContext wac;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.wac.getServletContext().setAttribute(("userSession"),new Session(new MockHttpSession()));
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        this.wac.getServletContext().setAttribute(("userSession"),new Session(new MockHttpSession()));

    }


    public ModelAndView getOverview(String url,Map<String,Object>parameters) throws Exception {
        MockHttpServletRequestBuilder requestAttributes = get(url).accept(MediaType.ALL).sessionAttr(("userSession"), new Session(new MockHttpSession()));
        if(parameters != null){
            for(String key:parameters.keySet()){
                requestAttributes.requestAttr(key,parameters.get(key));
            }
        }
        ResultActions a =  this.mockMvc.perform(requestAttributes);
        return  a.andReturn().getModelAndView();
    }
    @Test
    public void dummy(){
        //do nothing
    }
}