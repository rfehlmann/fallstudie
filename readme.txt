@Changelog from 07.01.2014
Directory
Doku ->                         Added new manual Testings
                                Added new Iterationplan with work balance

src/test/java ->                Added new Testings
src/main/java ->                Added new functions and files incl. updates

------------------------------------------

Willkommen beim Projekt "Room-Reservation"
Im Ordner Doku befindet sich das Protokoll des Kickoff-Meetings und weitere Planungsdokumente, welche mit dem Kunden und auch intern abgehalten wurden.
Tasks Iteration 1 = 2te Iteration. In der ersten wurden Grundstrukturen aufgebaut und Sitzungen mit dem Kunden abgehalten.

Prerequisites:

Installed plugins:
git,maven

IDE: IntelliJ

Frameworks:
Spring MVC, Gumby, Hibernate

Installation:

Für eine einheitliche Source Control wird ein zentrales GIT-Repository auf www.bitbucket.org genutzt, welches allen Projektmitgliedern zugänglich ist. 
Als Build-Tool wird im Team "Maven" genutzt.
Damit alle Projektmitlgieder die Sourcen gleich kompilieren können, wird darauf gesetzt, dass alle Mitglieder ein Linux System nutzen und als IDE die Software "IntelliJ" einsetzen.
Maven und IntelliJ stellen sicher, dass alle Mitglieder des Projektes alle diesselben 3th Party Libraries besitzn und nutzen.

Detailliertere Informationen zum Projekt können bei einem Mitglied angefragt werden.

Zusätzlich wurde ein Webserver mit Teamcity bestückt für die koordinierte Abarbeitung der Tasks und Stories. Ebenfalls ist damit ein Bugtrackingsystem implementiert.
Link: http://pitsch.dyndns.org:8111/
Login mit folgenden Daten:
User: guest
Pass: guest

Auf Teamcity wird zusätzlich bei jedem Commit im Git ein neues Build erzeugt, welches dann als einzelne Dateien oder als Gesamtes ZIP File heruntergeladen werden können
