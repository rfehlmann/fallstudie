<%--
  Created by IntelliJ IDEA.
  User: raphael
  Date: 17.11.13
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="header">
    <div class="row">
        <div class="twelve column ">Zimmer</div>
        <div>
        </div>
    </div>
</div>
<div id="gridBody">
    <c:forEach items="${rooms}" var="room">
        <div class="row">
            <div class="twelve column ">
                    ${room.value}
            </div>
        </div>
        <div>
            <button class="medium primary btn" id="edit" onclick="loadRoom(${room.key})">edit
            </button>
        </div>
    </c:forEach>
</div>