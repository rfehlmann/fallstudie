<%--
  Created by IntelliJ IDEA.
  User: raphael
  Date: 17.11.13
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div id="header">
    <div class="row">
        <div class="two column">
            <button id="prevDate" class="medium primary btn" onclick="loadOverview(-1,${timeStamp})">Vorheriger Tag
            </button>
        </div>
        <div class="two column">
            ${date}
        </div>
        <div class="two column">
            <button class="medium primary btn" id="nextDate" onclick="loadOverview(1,${timeStamp})">N&auml;chsterTag
            </button>
        </div>
    </div>
    <div class="row">
        <div class="twelve column ">Zimmer</div>
        <c:forEach begin="0" end="23" varStatus="loop">
            <div class="twelve column gridColumn">
            ${loop.index}
            </div>
        </c:forEach>
    </div>
</div>
<div id="gridBody">
    <c:forEach items="${reservations}" var="reservation">
        <div class="row">
            <div class="twelve column ">
                    ${reservation.roomName}
            </div>
            <c:forEach begin="0" end="23" varStatus="loop">
                <div class="twelve column gridColumn">

                    <c:if test='${ false == reservation.startEndMapping.containsKey(loop.index)}'>
                        <div>frei</div>
                    </c:if>
                    <c:if test='${ true == reservation.startEndMapping.containsKey(loop.index)}'>
                        <div>besetzt</div>
                    </c:if>
                </div>
            </c:forEach>

        </div>
    </c:forEach>
</div>