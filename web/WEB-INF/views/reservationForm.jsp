<%--
  Created by IntelliJ IDEA.
  User: raphael
  Date: 14.12.13
  Time: 21:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div id="message"></div>
<form id="reservationForm" onload="">
    <li class="prepend field">
        <span class="adjoined">Datum</span>
        <input id="date" name="date" type="text" required>
    </li>
    <li class="field">
        <span class="adjoined">Raum</span>

        <div class="picker">
            <select id="room" name="room" required>
                <option></option>
                <c:forEach items="${rooms}" var="room">
                    <option id="${room.key}" name="roomId">${room.value}</option>
                </c:forEach>

            </select>
        </div>
    </li>
    <li class="field">
        <span class="adjoined">Start Zeit</span>

        <div class="picker">
            <select id="startDate" name="startDate" required>
            <c:forEach begin="0" end="23" varStatus="loop">
                    <option>${loop.index}</option>
                </c:forEach>
            </select>

        </div>
    </li>
    <li class="field">
        <span class="adjoined">End Zeit</span>

        <div class="picker">
            <select id="endDate" name="endDate" required>
            <c:forEach begin="0" end="23" varStatus="loop">
                    <option>${loop.index}</option>
                </c:forEach>
            </select>

        </div>
    </li>
    <div class="medium default btn"><input type="button" value="Absenden" onclick="$('#reservationForm').submit()"/>
    </div>
</form>

<!--show datepicker-->
<script type="application/javascript">
    $("#date").datepicker();
    //add validation function
    validateFrom('#reservationForm');
</script>
