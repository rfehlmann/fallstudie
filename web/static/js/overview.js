/**
 * Created with IntelliJ IDEA.
 * User: raphael
 * Date: 17.11.13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */

function loadOverview(date, timeStamp) {
    var url = "/fallstudie/overview";
    if (date != 0) {
        url += "?startDate=" + date + "&currentDate=" + timeStamp;
    }
    $('#content').load(url);
}

