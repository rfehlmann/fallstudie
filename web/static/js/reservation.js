/**
 * Created by raphael on 14.12.13.
 */

function validateFrom(form) {
    $(form).validation({
        // pass an array of required field objects
        required: [

            {
                id: "startDate",
                // pass a function to the validate property for complex custom validations
                // the function will receive the jQuery element itself, return true or false depending on validation
                validate: function ($el) {
                    alert(parseInt($("#startDate").val()));
                    alert(parseInt($("#endDate").val()));
                    alert(parseInt($("#endDate").val()) >= parseInt($("#startDate").val()));
                    return parseInt($("#endDate").val()) >= parseInt($("#startDate").val());
                }
            }
        ],
        // callback for failed validaiton on form submit
        fail: function () {
            alert("Fehler bei der validirung der Daten")
        },
        // callback for successful validation on form submit
        // if omited, form will submit normally
        submit: function (data) {
            var data = "date=" + $("#date").val() + "&roomId=" + $("#room").children(":selected").attr("id") + "&startTime=" + $("#startDate").val() + "&endTime=" + $("#endDate").val();
            $.ajax({
                url: '/fallstudie/saveReservation',
                type: "post",
                data: data,
                async: false,

                success: function (mes) {
                    $("#message").html(mes);
                }
            });
        }
    });
}