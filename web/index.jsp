<%--
  Created by IntelliJ IDEA.
  User: raphael
  Date: 07.11.13
  Time: 19:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">

    <!-- Use the .htaccess and remove these lines to avoid edge case issues.
             More info: h5bp.com/b/378 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

    <!-- We highly recommend you use SASS and write your custom styles in sass/_custom.scss.
         However, there is a blank style.css in the css directory should you prefer -->
    <link rel="stylesheet" href="/fallstudie/static/frameworks/gumby.css">

    <link rel="stylesheet" href="/fallstudie/static/frameworks/style.css">
    <!-- <link rel="stylesheet" href="css/style.css"> -->

    <script src="/fallstudie/static/frameworks/modernizr-2.6.2.min.js"></script>


    <!-- Grab Google CDN's jQuery, fall back to local if offline -->
    <!-- 2.0 for modern browsers, 1.10 for .oldie -->
    <script>
        var oldieCheck = Boolean(document.getElementsByTagName('html')[0].className.match(/\soldie\s/g));
        if (!oldieCheck) {
            document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"><\/script>');
        } else {
            document.write('<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"><\/script>');
        }
    </script>
    <script>
        if (!window.jQuery) {
            if (!oldieCheck) {
                document.write('<script src="/fallstudie/static/frameworks/jquery-2.0.2.min.js"><\/script>');
            } else {
                document.write('<script src="/fallstudie/static/frameworks/jquery-1.10.1.min.js"><\/script>');
            }
        }
    </script>

    <!--
    Include gumby.js followed by UI modules followed by gumby.init.js
    Or concatenate and minify into a single file -->
    <script gumby-touch="/fallstudie/static/frameworks/" src="/fallstudie/static/frameworks/gumby.js"></script>
    <script src="/fallstudie/static/frameworks/ui/gumby.retina.js"></script>
    <script src="/fallstudie/static/frameworks/ui/gumby.fixed.js"></script>
    <script src="/fallstudie/static/frameworks/ui/gumby.skiplink.js"></script>
    <script src="/fallstudie/static/frameworks/ui/gumby.toggleswitch.js"></script>
    <script src="/fallstudie/static/frameworks/ui/gumby.checkbox.js"></script>
    <script src="/fallstudie/static/frameworks/ui/gumby.radiobtn.js"></script>
    <script src="/fallstudie/static/frameworks/ui/gumby.tabs.js"></script>
    <script src="/fallstudie/static/frameworks/ui/gumby.navbar.js"></script>
    <script src="/fallstudie/static/frameworks/ui/jquery.validation.js"></script>
    <script src="/fallstudie/static/frameworks/gumby.init.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <!--
    gumby.min.js contains gumby.js, all UI modules and gumby.init.js
    <script src="/fallstudie/static/frameworks/gumby.min.js"></script> -->
    <script src="/fallstudie/static/frameworks/plugins.js"></script>
    <script src="/fallstudie/static/frameworks/main.js"></script>
    <!--custom js includes-->
    <script src="/fallstudie/static/js/overview.js"></script>
    <script src="/fallstudie/static/js/standard.js"></script>
    <script src="/fallstudie/static/js/reservation.js"></script>
</head>


<script>
    $(document).ready(function () {
        //test code
        //$("#content").load("/fallstudie/overiew")
        loadOverview(0);
    })
</script>
</body>
<div id="navigation">
    <div class="row navbar pretty" id="nav1">
        <!-- Toggle for mobile navigation, targeting the <ul> -->

        <ul class="eight columns">
            <li><a onclick="loadContent('/fallstudie/showrooms')">R&auml;ume</a></li>
            <li><a onclick="loadContent('/fallstudie/showaddfeservationform')">Reservation</a></li>
            <li><a onclick="loadContent('/fallstudie/overview')">&Uuml;bersicht</a></li>

        </ul>

    </div>

</div>
<div id="content"></div>
</html>